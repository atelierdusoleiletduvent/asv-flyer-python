# coding=utf-8

from fpdf import FPDF
import unidecode


# pdf_w=148
# pdf_h=210

# A4
pdf_w=210
pdf_h=297

ANNEE = "2022"
PERIODE = "Octobre à Décembre"
TITRE = "autonomie / éco-habitat / artisanat"

CONTENT = {
    f'STAGES : {PERIODE}': 
        [
            { 
            'titre': "Initiation à la menuiserie - niveau 1",
            'dates_et_details': "dimanche 9 / samedi 15 octobre / samedi 19 novembre / samedi 3 décembre",
            'tarifs': 'à partir de 50€'
            },
            { 
            'titre': "Briques de terre crue : montage de cloison et enduits",
            'dates_et_details': "samedi 15 octobre / dimanche 6 novembre / dimanche 4 décembre",
            'tarifs': '90€'
            },
            { 
            'titre': "Initiation à la menuiserie - niveau 2",
            'dates_et_details': "samedi 15 octobre / dimanche 20 novembre / dimanche 4 décembre",
            'tarifs': 'à partir de 50€'
            },
            { 
            'titre': "Introduction à l'autonomie énergétique",
            'dates_et_details': "dimanche 16 octobre / dimanche 11 décembre",
            'tarifs': 'à partir de 50€'
            },
            { 
            'titre': "S'initier à la soudure / après-midi",
            'dates_et_details': "samedi 22 octobre",
            'tarifs': '60€'
            },
            { 
            'titre': "Fabrication de petit mobilier bois-métal",
            'dates_et_details': "dimanche 23 octobre",
            'tarifs': 'à partir de 65€'
            },
            { 
            'titre': "Gestion et entretien d'une parcelle forestière",
            'dates_et_details': "samedi 5 novembre / samedi 3 décembre",
            'tarifs': 'à partir de 30€'
            },
            { 
            'titre': "Démarrer son projet de Poêle de Masse",
            'dates_et_details': "dimanche 6 novembre / samedi 10 décembre",
            'tarifs': '120€'
            },
            { 
            'titre': "Fabriquer un cuiseur à bois type rocket-stove",
            'dates_et_details': "samedi 10 décembre",
            'tarifs': '195€'
            },
            { 
            'titre': "Fabriquer un Tank-Drum",
            'dates_et_details': "Week-end du 5-6 novembre",
            'tarifs': '240€'
            },
            { 
            'titre': "Panneaux solaires photovoltaïques en auto-consommation",
            'dates_et_details': "vendredi 18 novembre",
            'tarifs': '140€'
            },
            { 
            'titre': "Chauffe-eau solaire en auto-installation",
            'dates_et_details': "vendredi 18 novembre",
            'tarifs': '140€'
            },
            { 
            'titre': "Installation photovoltaïque autonome - conception",
            'dates_et_details': "lundi 21 novembre",
            'tarifs': '140€'
            },
            { 
            'titre': "Les composites isolants biosourcés - initiation",
            'dates_et_details': "samedi 26 novembre",
            'tarifs': '115€'
            },
            { 
            'titre': "S'initier au travail de l'acier ",
            'dates_et_details': "samedi 26 novembre",
            'tarifs': '120€'
            },
            { 
            'titre': "Atelier de réparation de vélo",
            'dates_et_details': "vendredi 2 décembre",
            'tarifs': 'adhésion 10€'
            },
        ],
    # 'ATELIERS PARTAGÉS : BOIS / MÉTAL': 
    # [
    #          {
    #              'titre': "Liste des créneaux & temps d'accompagnement",
    #              'dates_et_details': "https://www.atelierdusoleiletduvent.org/calendrier-ateliers",
    #              'tarifs':"adhésion à 10€"
    #          },
    # ]
}

# CONTENT = {
#     'STAGES : Artisanat bois et métal': 
#         [
#             {
#                         'titre':"S'initier à la soudure / matinée ou après-midi",
#                         'dates_et_details':"Samedi 23 Avril",
#                         'tarifs':'60 €'
#             },
#             {
#                         'titre':"Construction d'une cariole à vélo ou à pied",
#                         'dates_et_details':"Week-end du 7-8 Mai",
#                         'tarifs':'290 €'
#             },
#             {
#                         'titre':"Découverte Tournage sur Bois",
#                         'dates_et_details':"13, 14 ou 15 Mai / 10, 11 ou 12 Juin / 14, 15 16 ou 17 Juillet",
#                         'tarifs':'prix libre'
#             },
#             {
#                         'titre':"Fabriquer un Tank-Drum",
#                         'dates_et_details':"Week-end du 14-15 Mai",
#                         'tarifs':'240 €'
#             },
#             {
#                         'titre':"S'initier au travail de l'acier ",
#                         'dates_et_details':"Samedi 21 Mai",
#                         'tarifs':'120 €'
#             },
#             {
#                         'titre':"Initiation à la menuiserie - niveau 1",
#                         'dates_et_details':"Dimanche 29 Mai",
#                         'tarifs':'prix libre'
#             },
#             {
#                         'titre':"Initiation à la menuiserie - niveau 2",
#                         'dates_et_details':"Samedi 4 Juin",
#                         'tarifs':'90 €'
#             },
#             {
#                         'titre':"Fabriquer un cuiseur à bois type rocket-stove",
#                         'dates_et_details':"Samedi 25 Juin",
#                         'tarifs':'195 €'
#             },
#         ],

#     'LES RENDEZ-VOUS': 
#     [
#             {
#                 'titre': "Visite Atelier et Présentation des activités",
#                 'dates_et_details': "Vendredi 6 Mai à 18h30 / vendredi 17 juin à 18h30",
#                 'tarifs':"sur inscription"
#             },
#             {
#                         'titre':"Atelier de réparation de vélo",
#                         'dates_et_details':"Dimanche 22 Mai",
#                         'tarifs':"adhésion 10€"
#             },


#     ]
# }

class PDF(FPDF):

    pass # nothing happens when it is executed

    def draw(self):
        self.set_title(title=f"ASV - Calendrier {PERIODE.upper()} {ANNEE}")

        # BACKGROUND WHITE
        self.set_fill_color(255, 255, 255)
        self.rect(0,0,pdf_w,pdf_h,style='F')

        self.set_auto_page_break(auto = True, margin = 0.0)
        
        # HEADER
        self.set_xy(10,0)
        self.image('logo.jpg',  link='https://www.atelierdusoleiletduvent.org', type='jpg', w=41, h=41)


        self.set_xy(51,10)
        self.set_font('Dosis-SemiBold', '', 15)
        # self.set_text_color(56, 71, 99) # blue
        self.set_text_color(222, 109, 7) # Orange / Red
        self.cell(w=87, h=10.0, align='C', txt=f"{TITRE.upper()}", border=0)

        self.set_xy(51,20)
        self.set_font('Dosis-Regular', '', 14)
        self.set_text_color(187, 187, 187)
        self.cell(w=87, h=10.0, align='C', txt="Lusignan 86600", border=0)


        # FOOTER
        # self.set_xy(45,pdf_h-43)
        # self.image('logo_fse.jpg',  link='', type='jpg', w=18)

        # self.set_xy(85,pdf_h-43)
        # self.image('logo_na.jpg',  link='', type='jpg', w=32)

        # self.set_xy(10,pdf_h-100)
        # self.set_font('Dosis-Regular', '', 10)
        # self.set_text_color(56, 71, 99)
        # footer_txt = "www.atelierdusoleiletduvent.org"
        # self.cell(w=pdf_w-80, h=5, align='C', txt=footer_txt, border=0)

        self.set_xy(10,pdf_h-95)
        self.set_font('Dosis-Regular', '', 10)
        # self.set_text_color(56, 71, 99) # Blue
        self.set_text_color(222, 109, 7) # Orange / Red
        footer_txt = "www.atelierdusoleiletduvent.org / 09 50 86 32 89 / atelierdusoleiletduvent@gmail.com"
        self.cell(w=pdf_w-80, h=5, align='C', txt=footer_txt, border=0)


        # CONTENU
        sections_start_x = 10
        sections_start_y = 30

        current_x = sections_start_x
        current_y = sections_start_y

        for titre_section, elems in CONTENT.items():

            current_x = sections_start_x
            current_y += 5

            # TITRE SECTION
            self.set_xy(current_x, current_y)

            self.set_font('Dosis-SemiBold', '', 13)
            # self.set_text_color(56, 71, 99) # Dark Grey
            self.set_text_color(222, 109, 7) # Orange / Red
            self.cell(w=(pdf_w-20), h=10.0, align='L', txt=f"{titre_section.upper()}", border=0)

            current_y += 2 # offset between section and its elems
            
            # DICTIONNAIRES
            for el in elems:
                
                current_x = sections_start_x
                current_y += 5.5
                
                h_l1 = 7.0

                # TITRE EVENEMENT
                price_w = 60
                self.set_xy(current_x,current_y)

                self.set_font('Dosis-Regular', '', 10)
                self.set_text_color(56, 71, 99)
                
                # moved in PRIX
                # self.cell(w=(pdf_w-(price_w+20)), h=h_l1, align='L', txt=f"{el['titre'].upper()}", border=0)

                # PRIX
                if "tarifs" in el:
                    self.cell(w=(pdf_w-(price_w+38)), h=h_l1, align='L', txt=f"{el['titre'].upper()}", border=0)

                    self.set_xy(current_x+(pdf_w-(price_w+38)),current_y)

                    self.set_font('Dosis-Light', '', 9)
                    self.set_text_color(56, 71, 99)
                    self.cell(w=(price_w), h=h_l1, align='L', txt=f"{el['tarifs']}", border=0)
                else:
                    self.cell(w=(pdf_w-20), h=h_l1, align='L', txt=f"{el['titre'].upper()}", border=0)

                # DATES et DETAILS
                current_x = sections_start_x
                current_y += 4
        
                self.set_xy(current_x,current_y)
                print(f"last (x,y) = ({current_x}, {current_y})")

                self.set_font('Dosis-Light', '', 9)
                self.set_text_color(56, 71, 99)
                self.cell(w=(pdf_w-20), h=6, align='L', txt=f"{el['dates_et_details']}", border=0)


pdf=PDF(orientation='P', unit='mm', format='A5')

pdf.add_font('Dosis-Regular', '', 'Dosis-Regular.ttf', uni=True)
pdf.add_font('Dosis-Light', '', 'Dosis-Light.ttf', uni=True)
pdf.add_font('Dosis-Bold', '', 'Dosis-Bold.ttf', uni=True)
pdf.add_font('Dosis-SemiBold', '', 'Dosis-SemiBold.ttf', uni=True)
pdf.add_font('Dosis-Medium', '', 'Dosis-Medium.ttf', uni=True)

pdf.add_page()
pdf.draw()

nom_fichier = f'{PERIODE} {ANNEE}'.replace(' ', '-').replace('/', '-').upper()

nom_sendinblue = unidecode.unidecode(nom_fichier.replace('-', '-')).lower()
pdf.output(f'ASV_AGENDA_{ANNEE}_{nom_fichier}.pdf','F')
pdf.output(f'asv-agenda{nom_sendinblue}.pdf','F')

import os

# os.system(f"sips -s format png agenda{nom_sendinblue}.pdf --out agenda{nom_sendinblue}.png")
# os.system(f"sips -s format jpeg ASV_AGENDA_{ANNEE}_{nom_fichier}.pdf --out ASV_AGENDA_{ANNEE}_{nom_fichier}.jpg")

os.system(f"convert -density 300 asv-agenda{nom_sendinblue}.pdf asv-agenda{nom_sendinblue}.png")
os.system(f"convert -density 300 ASV_AGENDA_{ANNEE}_{nom_fichier}.pdf ASV_AGENDA_{ANNEE}_{nom_fichier}.jpg")

